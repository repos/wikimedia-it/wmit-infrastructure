#!/bin/sh
# Authors: Valerio Bozzolan and contributors
# Credits: https://www.mediawiki.org/wiki/Manual:Job_queue
# Source:  https://gitlab.wikimedia.org/repos/wikimedia-it/wmit-infrastructure/-/tree/main/servers/intreccio/projects/wikina
# License: CC BY-SA 3-0
# Year: 2022

# die in case of any error
set -e

# MediaWiki installation path
# it can be customized using an env variable or as first argument in the command line
if [ -z "$IP" ]; then
	IP="$1"
fi

if [ -z "$IP" ]; then
	echo "No Mediawiki installation pathname - no party"
	exit 2
fi

# try to enter in this directory
cd "$IP"

# set a default startup pause in seconds
# it can be customized using an env variable
if [ -z "$STARTUP_WAIT" ]; then
	STARTUP_WAIT=0
fi

# set a default pause interval in seconds
# it can be customized using an env variable
if [ -z "$INTERVAL" ]; then
	INTERVAL=8
fi

# set a default maximum number of jobs
# it can be customized using an env variable
if [ -z "$MAX_JOBS" ]; then
	MAX_JOBS=20
fi

# prepare the execution job command
EXEC_JOBS="php --file $IP/maintenance/runJobs.php -- --conf $IP/LocalSettings.php"

# Wait a minute after the server starts up to give other processes time to get started
if [ "$STARTUP_WAIT" -gt 0 ]; then
	echo "Starting job service in $STARTUP_WAIT seconds"
	sleep $STARTUP_WAIT
fi

echo "Started running max $EXEC_JOBS jobs"

while :; do
	# Job types that need to be run ASAP mo matter how many of them are in the queue
	# Those jobs should be very "cheap" to run
	$EXEC_JOBS --type=enotifNotify

	# Everything else, limit the number of jobs on each batch
	# The --wait parameter will pause the execution here until new jobs are added,
	# to avoid running the loop without anything to do
	$EXEC_JOBS --wait --maxjobs="$MAX_JOBS"

	# Wait some seconds to let the CPU do other things, like handling web requests, etc
	echo "Wait $INTERVAL seconds"
	sleep $INTERVAL
done
