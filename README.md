# Wikimedia IT's infrastructure

This repository contains some public configuration related to Wikimedia Italia servers.

The purpose is:

* share resources
* track changes
* blame regressions
* etc.

## Workboard

https://phabricator.wikimedia.org/tag/wmit-infrastructure/

## Information

https://wiki.wikimedia.it/wiki/Infrastruttura

## License

Unless otherwise noted, contents in this repository should be considered in the public domain. In fact, this repository should not consist in source code, documents, images, but server configurations ineligible for copyright, lacking in threshold of creativity.

For any legal question feel free to contact Wikimedia Italia:

```
segreteria@wikimedia.it
wikimediaitalia@pec
Via Bergognone, 34 – 20144 Milano (MI)
https://www.wikimedia.it/
```
